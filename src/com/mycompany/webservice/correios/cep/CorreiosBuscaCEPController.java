/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.webservice.correios.cep;

import br.com.correios.bsb.sigep.master.bean.cliente.EnderecoERP;
import br.com.correios.bsb.sigep.master.bean.cliente.SQLException_Exception;
import br.com.correios.bsb.sigep.master.bean.cliente.SigepClienteException;
import com.mycompany.webservice.correios.cep.util.Alerts;
import com.mycompany.webservice.correios.cep.util.TextFieldFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Edgard Monção
 */
public class CorreiosBuscaCEPController {

    @FXML
    private Label lbResult;

    @FXML
    private TextField txtCEP;

    @FXML
    private Button btnConsulta;
    
    @FXML
    void btnConsultarAction() {
        br.com.correios.bsb.sigep.master.bean.cliente.AtendeClienteService service = new br.com.correios.bsb.sigep.master.bean.cliente.AtendeClienteService();
        br.com.correios.bsb.sigep.master.bean.cliente.AtendeCliente port = service.getAtendeClientePort();
        
        try {
            String cep = txtCEP.getText();
            br.com.correios.bsb.sigep.master.bean.cliente.EnderecoERP result = port.consultaCEP(cep);
            lbResult.setText(result.getEnd() + " - " + result.getBairro() + " - " + result.getCidade() + "/" + result.getUf());
        }catch(NumberFormatException e){
            Alerts.showAlertt("Error", "Parse error", e.getMessage(), Alert.AlertType.ERROR);
        } catch (SQLException_Exception | SigepClienteException ex) {
            Logger.getLogger(CorreiosBuscaCEPController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    private void campoFormatado() {
        TextFieldFormatter mask = new TextFieldFormatter();
        mask.setMask("##.###-###");
        mask.setCaracteresValidos("0123456789");
        mask.setTf(txtCEP);
        mask.formatter();
    }
        
}
